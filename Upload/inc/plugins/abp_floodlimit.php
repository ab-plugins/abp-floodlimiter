<?php

/**
 * Allow to limit number of posts per hour
 * Copyright 2021 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

define('CN_ABPFLOODLIMIT', str_replace('.php', '', basename(__FILE__)));

/**
 * Informations about the plugin
 * @global MyLanguage $lang
 * @return array
 */
function abp_floodlimit_info() {
    global $lang;
    $lang->load(CN_ABPFLOODLIMIT);
    return array(
        'name' => $lang->abp_floodlimit_name,
        'description' => $lang->abp_floodlimit_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-floodlimiter',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.0',
        'compatibility' => '18*',
        'codename' => CN_ABPFLOODLIMIT
    );
}

/**
 * 
 * @global DB_Base $db
 * @global MyLanguage $lang
 */
function abp_floodlimit_install() {
    global $db, $lang;
    $lang->load(CN_ABPFLOODLIMIT);
    $settinggroups = [
        'name' => CN_ABPFLOODLIMIT,
        'title' => $lang->abp_floodlimit_name,
        'description' => $lang->abp_floodlimit_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    $settings = [
        [
            'gid' => $gid,
            'name' => CN_ABPFLOODLIMIT . '_groups',
            'title' => $lang->abp_floodlimit_groups,
            'description' => $lang->abp_floodlimit_groups_desc,
            'optionscode' => 'groupselect',
            'value' => '1,2,5,7',
            'disporder' => 1
        ],
        [
            'gid' => $gid,
            'name' => CN_ABPFLOODLIMIT . '_nbposts',
            'title' => $lang->abp_floodlimit_nbposts,
            'description' => $lang->abp_floodlimit_nbposts_desc,
            'optionscode' => 'numeric' . PHP_EOL . 'min=1',
            'value' => '5',
            'disporder' => 2
        ]
    ];
    $db->insert_query_multiple('settings', $settings);
    rebuild_settings();
}

function abp_floodlimit_is_installed() {
    global $mybb;
    return (array_key_exists(CN_ABPFLOODLIMIT . '_nbposts', $mybb->settings));
}

function abp_floodlimit_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPFLOODLIMIT . "%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPFLOODLIMIT . "'");
    rebuild_settings();
}

$plugins->add_hook('datahandler_post_validate_post', 'abp_floodlimit_postcount');
$plugins->add_hook('datahandler_post_validate_thread', 'abp_floodlimit_postcount');

/**
 * 
 * @global type $mybb
 * @global DB_Base $db
 * @global MyLanguage $lang
 * @param type $datahandler
 * @return type
 */
function abp_floodlimit_postcount(&$datahandler) {
    global $mybb, $db, $lang;
    if (!isset($datahandler->data['message'])) {
        return;
    }
    if (!isset($datahandler->data['uid']) && isset($datahandler->data['edit_uid'])) {
        $puser = get_user($datahandler->data['edit_uid']);
    } else {
        $puser = get_user($datahandler->data['uid']);
    }
    if (!is_member($mybb->settings[CN_ABPFLOODLIMIT . '_groups'], $puser['uid'])) {
        return;
    }
    $query = $db->simple_select('posts', 'COUNT(pid) AS nbposts', "uid=" . (int) $puser['uid'] . " AND dateline>=(UNIX_TIMESTAMP()-3600)");
    $nbposts = $db->fetch_field($query, 'nbposts');
    if ((int) $nbposts >= (int) $mybb->settings[CN_ABPFLOODLIMIT . '_nbposts']) {
        $lang->load(CN_ABPFLOODLIMIT);
        $datahandler->set_error($lang->abp_floodlimit_posts);
    }
}
