<?php
/**
 * Allow to limit number of posts per hour
 * Copyright 2021 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

$l['abp_floodlimit_name'] = 'ABP Flood Limiter';
$l['abp_floodlimit_desc'] = 'Allows to limit number of post per hour';
$l['abp_floodlimit_setting_desc'] = 'Settings for ABP Flood Limiter';
$l['abp_floodlimit_groups'] = 'Target groups';
$l['abp_floodlimit_groups_desc'] = 'Select groups which are limited';
$l['abp_floodlimit_nbposts'] = 'Max posts';
$l['abp_floodlimit_nbposts_desc'] = 'Number of posts allowed per hour';