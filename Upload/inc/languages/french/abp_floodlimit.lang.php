<?php
/**
 * Allow to limit number of posts per hour
 * Copyright 2021 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

$l['abp_floodlimit_posts'] = 'Désolé mais vous avez dépassé la limite de posts pour cette heure.';