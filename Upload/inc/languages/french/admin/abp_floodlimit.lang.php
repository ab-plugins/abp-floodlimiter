<?php
/**
 * Allow to limit number of posts per hour
 * Copyright 2021 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

$l['abp_floodlimit_name'] = 'ABP Flood Limiter';
$l['abp_floodlimit_desc'] = 'Permet de limiter le nombre de posts par heure';
$l['abp_floodlimit_setting_desc'] = 'Réglages pour ABP Flood Limiter';
$l['abp_floodlimit_groups'] = 'Groupes ciblés';
$l['abp_floodlimit_groups_desc'] = 'Choisissez les groupes qui seront limités';
$l['abp_floodlimit_nbposts'] = 'Posts max';
$l['abp_floodlimit_nbposts_desc'] = 'Nombre de posts autorisés par heure';