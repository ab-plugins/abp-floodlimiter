# ABP Flood Limiter

This plugin allows to specify how much post can be done per hour on your forum.

## Installation
- Extract the archive
- Upload the content of Upload/ directory to your forum root
- *Install and activate*
- Change settings

## Settings
Only two:
- Targeted groups
- Number of post allowed per hour